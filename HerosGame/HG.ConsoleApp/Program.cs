using HG.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HG.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string result;
            do
            {
                Console.WriteLine("To start new game press [Y], to exit press [X]");
                result = Console.ReadLine();
                switch (result.ToLower())
                {
                    case "y":
                        {
                            Hero firstPlayer = ChoosePlayer("First");
                            Hero secondPlayer = ChoosePlayer("Second");
                            GameEngine game = new GameEngine();

                            firstPlayer.AttackStopped += DisplayMessageForZeroDamageTaken;
                            secondPlayer.AttackStopped += DisplayMessageForZeroDamageTaken;

                            game.GameStarted += DisplayMessage;
                            game.DamageTaken += DisplayDamageTaken;
                            game.GameEnded += DisplayMessage;

                            game.Fight(firstPlayer, secondPlayer);
                            break;
                        }
                    case "x":
                        {
                            return;
                        }
                    default:
                        Console.WriteLine("Not a valid option.");

                        break;

                }
            } while (result.ToLower() != "x");

            Hero ChoosePlayer(string player)
            {
                Hero hero = null;
                int type;
                string name;

                do
                {
                    Console.WriteLine("Choose " + player + " player: ");
                    Console.WriteLine("1. Warrior 2. Knight 3. Assassin 4. Monk");

                    int.TryParse(Console.ReadLine(), out type);
                    switch (type)
                    {
                        case 1:
                            {
                                name = ChooseNameOfPlayer(player);
                                hero = new Warrior(name);
                                break;
                            }
                        case 2:
                            {
                                name = ChooseNameOfPlayer(player);
                                hero = new Knight(name);
                                break;
                            }
                        case 3:
                            {
                                name = ChooseNameOfPlayer(player);
                                hero = new Assassin(name);
                                break;
                            }
                        case 4:
                            {
                                name = ChooseNameOfPlayer(player);
                                hero = new Monk(name);
                                break;
                            }
                        default:
                            Console.Write("Not a valid option. ");
                            break;
                    }
                } while (type != 1 && type != 2 && type != 3 && type != 4);

                return hero;
            }

            string ChooseNameOfPlayer(string player)
            {
                string name;
                do
                {
                    Console.WriteLine("Write name for " + player + " player");
                    name = Console.ReadLine();
                    if (name != null && name != "")
                        break;
                    else Console.WriteLine("Value cannot be null or empty.");

                } while (string.IsNullOrEmpty(name));
                return name;
            }

            void DisplayDamageTaken(object sender, TakenDamageEventArgs e)
            {
                Console.WriteLine(e.Name + " has taken " + Math.Round(e.TakenDamage, 0) + " p damage");
            }

            void DisplayMessage(object sender, SendMessageEventArgs e)
            {
                if (e.MessageStaus == SendMessageEventArgs.Message.GameStarted)
                    Console.WriteLine("The game is afoot! May the odds be ever in your favour!");
                if (e.MessageStaus == SendMessageEventArgs.Message.GameEnded)
                    Console.WriteLine(e.Name + " has lost the game");
            }

            void DisplayMessageForZeroDamageTaken(object sender, StoppedAttackEventArgs e)
            {
                if (e.StoppedAttackMethod == StoppedAttackEventArgs.StoppedAttack.Avoided)
                    Console.Write("Attack was avoided. ");
                else if (e.StoppedAttackMethod == StoppedAttackEventArgs.StoppedAttack.Blocked)
                    Console.Write("Attack was blocked. ");
                else if (e.StoppedAttackMethod == StoppedAttackEventArgs.StoppedAttack.WardedOff)
                    Console.Write("Attack was warded off. ");
            }
        }
    }
}
