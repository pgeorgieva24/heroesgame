﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HG.Data
{
    public class Monk : Hero
    {
        public Monk(string name) : base(name)
        {
            HealthPoints = 200;
            AttackPoints = 30;
            ArmorPoints = 20;
        }

        public override double Deffend(double damagePoints)
        {
            if (IsThereAChanceByPersentage(30))
            {
                OnAttackAvoided();
                return base.Deffend(0);
            }
            return base.Deffend(damagePoints);
        }
    }
}
