﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HG.Data
{
    public class StoppedAttackEventArgs : EventArgs
    {
        public StoppedAttack StoppedAttackMethod { get; set; }

        public enum StoppedAttack
        {
            Avoided, Blocked, WardedOff
        }
    }
}
