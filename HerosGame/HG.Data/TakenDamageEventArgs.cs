﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HG.Data
{
    public class TakenDamageEventArgs : EventArgs
    {
        public string Name { get; set; }

        public double TakenDamage { get; set; }
    }
}
