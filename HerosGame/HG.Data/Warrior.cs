﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HG.Data
{
    public class Warrior : Hero
    {
        public Warrior(string name) : base(name)
        {
            HealthPoints = 200;
            AttackPoints = 28;
            ArmorPoints = 22;
        }
    }
}
