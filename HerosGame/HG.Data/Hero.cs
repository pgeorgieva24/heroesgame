﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HG.Data
{
    public abstract class Hero
    {
        private Random random = new Random();
        private double healthPoints;
        private double attackPoints;
        private double armorPoints;
        private string name;

        public event EventHandler<StoppedAttackEventArgs> AttackStopped;


        public Hero(string name) { Name = name; }

        public double HealthPoints
        {
            get { return healthPoints; }
            set
            {
                if (value > 0)
                {
                    healthPoints = value;
                }
                else throw new ArgumentOutOfRangeException("Value for health points must be greater than 0");

            }
        }

        public double AttackPoints
        {
            get { return attackPoints; }
            set
            {
                if (value > 0)
                {
                    attackPoints = value;
                }
                else throw new ArgumentOutOfRangeException("Value for attack points must be greater than 0");
            }
        }
        public string Name
        {
            get { return name; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    name = value;
                }
                else throw new ArgumentException("Value of name can't be null or empty");
            }
        }

        public double ArmorPoints
        {
            get { return armorPoints; }
            set
            {
                if (value > 0)
                {
                    armorPoints = value;
                }
                else throw new ArgumentOutOfRangeException("Value for armor points must be greater than 0");
            }
        }

        public bool IsDead { get; private set; }

        public virtual double Attack()
        {
            return (AttackPoints * (random.Next(80, 120) / 100.0));
        }

        public virtual double Deffend(double damagePoints)
        {
            double takenDamage = damagePoints - (ArmorPoints * ((double)random.Next(80, 120) / 100));

            if (takenDamage < 0)
                takenDamage = 0;

            if (takenDamage == 0 && damagePoints != 0)
                OnAttackWardedOff();
            try
            {
                HealthPoints -= takenDamage;
            }
            catch (ArgumentOutOfRangeException)
            {
                IsDead = true;
            }

            return takenDamage;
        }

        protected bool IsThereAChanceByPersentage(int percentage)
        {
            int randomNumber = random.Next(1, 100);
            if (randomNumber < percentage + 1)
                return true;
            return false;
        }

        protected virtual void OnAttackAvoided()
        {
            AttackStopped?.Invoke(this, new StoppedAttackEventArgs() { StoppedAttackMethod = StoppedAttackEventArgs.StoppedAttack.Avoided });
        }

        protected virtual void OnAttackBlocked()
        {
            AttackStopped?.Invoke(this, new StoppedAttackEventArgs() { StoppedAttackMethod = StoppedAttackEventArgs.StoppedAttack.Blocked });
        }

        protected virtual void OnAttackWardedOff()
        {
            AttackStopped?.Invoke(this, new StoppedAttackEventArgs() { StoppedAttackMethod = StoppedAttackEventArgs.StoppedAttack.WardedOff });
        }
    }
}
