﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HG.Data
{
    public class Knight : Hero
    {
        public Knight(string name) : base(name)
        {
            HealthPoints = 250;
            AttackPoints = 35;
            ArmorPoints = 20;
        }

        public override double Attack()
        {
            double rawDamage = base.Attack();
            if (IsThereAChanceByPersentage(10))
            {
                rawDamage = rawDamage * 2;
            }
            return rawDamage;
        }

        public override double Deffend(double damagePoints)
        {
            if (IsThereAChanceByPersentage(20))
            {
                OnAttackBlocked();
                return base.Deffend(0);
            }
            return base.Deffend(damagePoints);
        }
    }
}
