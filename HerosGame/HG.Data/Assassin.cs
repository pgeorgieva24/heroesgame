﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HG.Data
{
    public class Assassin : Hero
    {
        public Assassin(string name) : base(name)
        {
            HealthPoints = 200;
            AttackPoints = 30;
            ArmorPoints = 20;
        }

        public override double Attack()
        {
            double rawDamage = base.Attack();
            if (IsThereAChanceByPersentage(30))
            {
                rawDamage = rawDamage * 3;
            }
            return rawDamage;
        }
    }
}
