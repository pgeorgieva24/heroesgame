﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HG.Data
{
    public class GameEngine
    {
        public event EventHandler<TakenDamageEventArgs> DamageTaken;
        public event EventHandler<SendMessageEventArgs> GameStarted, GameEnded;

        public void Fight(Hero firstPlayer, Hero secondPlayer)
        {
            double rawDamage;

            OnGameStarted();

            while (!firstPlayer.IsDead && !secondPlayer.IsDead)
            {
                rawDamage = firstPlayer.Attack();
                OnDamageTaken(secondPlayer.Deffend(rawDamage), secondPlayer.Name);

                if (secondPlayer.IsDead)
                {
                    OnGameEnded(secondPlayer.Name);
                    break;
                }

                rawDamage = secondPlayer.Attack();
                OnDamageTaken(firstPlayer.Deffend(rawDamage), firstPlayer.Name);

                if (firstPlayer.IsDead)
                {
                    OnGameEnded(firstPlayer.Name);
                    break;
                }
            }
        }

        protected virtual void OnGameStarted()
        {
            GameStarted?.Invoke(this, new SendMessageEventArgs() { MessageStaus = SendMessageEventArgs.Message.GameStarted });
        }

        protected virtual void OnDamageTaken(double takenDamage, string name)
        {
            DamageTaken?.Invoke(this, new TakenDamageEventArgs() { TakenDamage = takenDamage, Name = name });
        }

        protected virtual void OnGameEnded(string nameOfDeadPlayer)
        {
            GameEnded?.Invoke(this, new SendMessageEventArgs() { MessageStaus = SendMessageEventArgs.Message.GameEnded, Name = nameOfDeadPlayer });
        }
    }
}
