﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HG.Data
{
    public class SendMessageEventArgs : EventArgs
    {
        public Message MessageStaus { get; set; }
        public string Name { get; set; }

        public enum Message
        {
            GameStarted,
            GameEnded
        }

    }
}
